
-- SUMMARY --

The Random Crash module randomly whitescreens your site when you are displaying nodes.
It will not affect admin pages.  An admin screen located at admin/config/random-crash allows
you to set the frequency of the whitescreening.  Options are 25%, 50%, 75% and 100%.


-- REQUIREMENTS --

Bad Judgement (http://drupal.org/project/bad_judgement)

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONTACT --

Current maintainers:
* David Makalsky (drupkick) - http://drupal.org/user/669834