<?php
/**
 * @file
 * Admin page callbacks for the random_crash module.
 */

/**
 * Administration settings form.
 *
 * @see system_settings_form()
 */
function _random_crash_admin_form(){
  $form = array();


  $form['crash_perc'] = array(
    '#type' => 'select',
    '#title' => t('Crash Percentage'),
    '#options' => array(
      25 => t('25%'),
      50 => t('50%'),
      75 => t('75%'),
      100 => t('100%'),
    ),
    '#default_value' => variable_get('crash_perc', 50),
    '#description' => t('How often do you want to whitescreen?'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Save the form submission into the variables table.
 * @param type $form
 * @param type $form_state 
 */
function _random_crash_admin_form_submit($form, &$form_state){
  variable_set('crash_perc', $form_state['values']['crash_perc']);
  drupal_set_message(t('Settings Saved.'));
}